#!/bin/bash -e

# Install paho.mqtt.c:
on_chroot << EOF
cd /home/pinner/
git clone https://github.com/eclipse/paho.mqtt.c.git
cd paho.mqtt.c
make
make install
cd ..
EOF

# Install PINS-BM:
on_chroot << EOF
cd /home/pinner/
git clone https://gitlab.com/DIY_Networking_Group_1/PINS-BM.git
cd PINS-BM/
git submodule init
git submodule update --recursive --remote
mkdir build
cd build
cmake .. -DRPI_BUILD=ON
make
make install
cd ../..
EOF

# Cleanup:
on_chroot << EOF
# rm -rf PINS-BM/
EOF

# Install PINS-BM service:
#install -m 644 files/pinns.service "${ROOTFS_DIR}/etc/systemd/system/"
#on_chroot << EOF
#systemctl daemon-reload
#systemctl enable pinns.service
#EOF
install -m 644 files/pinscron "${ROOTFS_DIR}"

on_chroot << EOF
    su pinns
    crontab /pinscron
    rm /pinscron -rf
EOF
