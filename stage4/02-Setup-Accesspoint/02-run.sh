#!/bin/bash -e

# Source: https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md

on_chroot << EOF
systemctl stop dnsmasq
systemctl stop hostapd
EOF

install -v -m 644 files/dhcpcd "${ROOTFS_DIR}"
on_chroot << EOF
cat /dhcpcd >> /etc/dhcpcd.conf
rm /dhcpcd
service dhcpcd restart
EOF

install -v -m 644 files/dnsmasq "${ROOTFS_DIR}"
on_chroot << EOF
mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
cat /dnsmasq >> /etc/dnsmasq.conf
rm /dnsmasq
EOF

install -v -m 644 files/hostapd "${ROOTFS_DIR}"
on_chroot << EOF
cat /hostapd >> /etc/hostapd/hostapd.conf
rm /hostapd
EOF

on_chroot << EOF
rpl '#DAEMON_CONF=""' 'DAEMON_CONF="/etc/hostapd/hostapd.conf"' /etc/default/hostapd
EOF

on_chroot << EOF
systemctl start hostapd
systemctl start dnsmasq
EOF

on_chroot << EOF
# rpl '#net.ipv4.ip_forward=1' 'net.ipv4.ip_forward=1' /etc/sysctl.conf
# iptables -t nat -A  POSTROUTING -o eth0 -j MASQUERADE
# sh -c "iptables-save > /etc/iptables.ipv4.nat"
EOF

install -v -m 644 files/rc "${ROOTFS_DIR}"
on_chroot << EOF
rpl 'exit 0' 'iptables-restore < /etc/iptables.ipv4.nat' /etc/rc.local
cat /rc >> /etc/rc.local
rm /rc
EOF
