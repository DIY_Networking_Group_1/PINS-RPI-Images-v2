#!/bin/bash -e

# Sunfounder Humiture Sensor (https://www.sunfounder.com/learn/sensor-kit-v2-0-for-raspberry-pi-b-plus/lesson-28-humiture-sensor-sensor-kit-v2-0-for-b-plus.html):
on_chroot << EOF
cd /home/pinner/
git clone https://github.com/sunfounder/Adafruit_Python_DHT.git
cd Adafruit_Python_DHT/
python setup.py install
EOF

# Sunfounder DS18B20 Temperature Sensor (https://www.sunfounder.com/learn/sensor-kit-v2-0-for-raspberry-pi-b-plus/lesson-26-ds18b20-temperature-sensor-sensor-kit-v2-0-for-b-plus.html):
on_chroot << EOF
echo 'dtoverlay=w1-gpio' >> /boot/config.txt
# sudo modprobe w1-gpio
# sudo modprobe w1-therm
EOF

on_chroot << EOF
pip3 install paho-mqtt
pip3 install RPi.GPIO
pip3 install get-mac
cd /home/pinner/
git clone https://gitlab.com/DIY_Networking_Group_1/PINS-Sensor.git
cp PINS-Sensor/gen/sensors.py /opt/
sudo chown pinner /opt/sensors.py
EOF

# Install PINS-BM service:
install -m 644 files/sensors.service "${ROOTFS_DIR}/etc/systemd/system/"
on_chroot << EOF
systemctl daemon-reload
systemctl enable sensors.service
EOF
