#!/bin/bash -e

# Install Wireless Tools:
on_chroot << EOF
cd/home/pinner/
wget https://hewlettpackard.github.io/wireless-tools/wireless_tools.29.tar.gz
tar -xzf wireless_tools.29.tar.gz
rm -rf wireless_tools.29.tar.gz
cd wireless_tools.29
make
make install
cd ..
EOF

# Compile and install PINS-PTM:
on_chroot << EOF
cd/home/pinner/
git clone https://gitlab.com/DIY_Networking_Group_1/PINS-PTM.git
cd PINS-PTM/
make init
make compilePi
make install
cd ..
EOF

# Install PINS-PTM system service:
install -m 644 files/pinsPtm.service "${ROOTFS_DIR}/etc/systemd/system/"
on_chroot << EOF
systemctl daemon-reload
systemctl enable pinsPtm.service
EOF
