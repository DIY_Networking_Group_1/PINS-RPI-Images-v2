#!/bin/bash -e

# Update package list and enable SSH:
on_chroot << EOF
apt update -y
sudo systemctl enable ssh
EOF
